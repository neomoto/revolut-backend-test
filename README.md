## Money transfer application (Revolut Backend Test)
Application provides API for transfer money between accounts. 

### Build and run

1) Build application with  
`gradle clean shadowJar`  
in a root directory.
2) Run it with  
`java -jar build\libs\transactions-1.0-SNAPSHOT-all.jar`

It is possible to specify external configuration with `config.location` property  
`java -jar build\libs\transactions-1.0-SNAPSHOT-all.jar config.location=~/application.properties`

Available properties:
>server.port      
>server.context

Default properties:
>server.port=8080    
>server.context=/api/v1

##Testing
Application covered with integration tests.  
Tests could be run with  
`gradle test`
in a root directory.


##API
All money movements happen by transactions. Transactions could be only created and can't be deleted or altered.

####Create transaction

`POST /api/v1/transaction`
Request body:
```json
{
  "amount": 8784.88,
  "fromAccountId": "eb79544a-0add-48a2-89ce-efe9c319e3af",   
  "toAccountId": "e5b8f197-205a-46d1-9ca9-13e0f888d6b5"     
}
```
Response:
```json
{
  "id": "8b9acbfa-b38b-4098-a784-91ac7dcbc94e",
  "amount": 8784.88,
  "fromAccountId": "eb79544a-0add-48a2-89ce-efe9c319e3af",
  "toAccountId": "e5b8f197-205a-46d1-9ca9-13e0f888d6b5"     
}
```
Error response:
```json
{
  "message": "not.enough.money",
  "code": 5004
}
```

#### Get list of all transactions
`GET /api/v1/transaction`  
Response:
```json
[
    {
      "id": "8b9acbfa-b38b-4098-a784-91ac7dcbc94e",
      "amount": 8784.88,
      "fromAccountId": "eb79544a-0add-48a2-89ce-efe9c319e3af",
      "toAccountId": "e5b8f197-205a-46d1-9ca9-13e0f888d6b5"     
    },
    {
      "id": "4599741a-86f8-4c53-b48e-c12334aab77a",
      "amount": 46.12,
      "fromAccountId": "e5b8f197-205a-46d1-9ca9-13e0f888d6b5",
      "toAccountId": "eb79544a-0add-48a2-89ce-efe9c319e3af"
    }
]
```

### Get account
`GET /api/v1/account/f65a373a-e9c9-49ba-9085-4b163d20b882`
Response:
```json
{
  "id": "f65a373a-e9c9-49ba-9085-4b163d20b882",
  "amount": 874.47     
}
```
Error response:
```json
{
  "message": "account.not.found",
  "code": 200
}
```
### Get all accounts
`GET /api/v1/account`
Response:
```json
[
  {
    "id": "f65a373a-e9c9-49ba-9085-4b163d20b882",
    "amount": 874.47     
  },
  {
    "id": "a963f6cb-5cba-414d-b98f-97d74bc995e4",
    "amount": 454.44
  }
]
```
