package com.revolut.transfer.controller;

import com.revolut.transfer.controller.dto.AccountDto;
import com.revolut.transfer.controller.dto.ErrorMessage;
import com.revolut.transfer.controller.exception.mapper.DaoExceptionMapper;
import com.revolut.transfer.controller.exception.mapper.ServiceExceptionMapper;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.ws.rs.core.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountControllerTest extends JerseyTest {
    @Override
    protected Application configure() {
        return new ResourceConfig(AccountController.class)
            .register(new ServiceExceptionMapper())
            .register(new DaoExceptionMapper());
    }

    @Test
    public void shouldGetAllAccountsExisted() {
        Response response = target("/account").request().get();

        assertThat(response.getStatus())
            .as("Http Response should be 200:")
            .isEqualTo(Response.Status.OK.getStatusCode());

        assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE))
            .as("Content type should be application/json:")
            .isEqualTo(MediaType.APPLICATION_JSON);

        List<AccountDto> content = response.readEntity(new GenericType<List<AccountDto>>() {
        });
        assertThat(content).size().isEqualTo(2);
    }

    @Test
    public void shouldReturnAccountById() {
        Response response = target("/account/cfc6ba0a-3051-4558-b110-6b8a751dfb6f").request().get();

        assertThat(response.getStatus())
            .as("Http Response should be 200:")
            .isEqualTo(Response.Status.OK.getStatusCode());

        assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE))
            .as("Content type should be application/json:")
            .isEqualTo(MediaType.APPLICATION_JSON);

        AccountDto accountDto = response.readEntity(AccountDto.class);
        assertThat(accountDto.getId()).isEqualTo(UUID.fromString("cfc6ba0a-3051-4558-b110-6b8a751dfb6f"));
        assertThat(accountDto.getAmount()).isEqualTo(BigDecimal.valueOf(500));
    }


    @Test
    public void shouldFailToFindAccount() {
        Response response = target("/account/4b75b263-54e6-4d0b-a78f-c8160952d340").request().get();

        assertThat(response.getStatus())
            .as("Http Response should be 400:")
            .isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        ErrorMessage errorMessage = response.readEntity(ErrorMessage.class);
        assertThat(errorMessage.getMessage())
            .as("Exception message should be:")
            .isEqualTo("account.not.found");
    }

}
