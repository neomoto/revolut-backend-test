package com.revolut.transfer.controller;

import com.revolut.transfer.controller.dto.AccountDto;
import com.revolut.transfer.controller.dto.ErrorMessage;
import com.revolut.transfer.controller.dto.TransactionDto;
import com.revolut.transfer.controller.exception.mapper.DaoExceptionMapper;
import com.revolut.transfer.controller.exception.mapper.ServiceExceptionMapper;
import com.revolut.transfer.dao.enums.AccountStateCode;
import org.assertj.core.api.SoftAssertions;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionControllerTest extends JerseyTest {
    @Override
    protected Application configure() {
        return new ResourceConfig(TransactionController.class)
                .register(AccountController.class)
                .register(new ServiceExceptionMapper())
                .register(new DaoExceptionMapper());
    }

    @Test
    public void shouldCreateTransaction() {
        Response response = target("/transaction")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new TransactionDto(null, BigDecimal.valueOf(54.4),
                                UUID.fromString("cfc6ba0a-3051-4558-b110-6b8a751dfb6f"),
                                UUID.fromString("0020f419-1fac-4de7-b6e7-a47de46126b1")),
                        MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatus())
                .as("Http Response should be 200:")
                .isEqualTo(Response.Status.OK.getStatusCode());

        assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE))
                .as("Content type should be application/json:")
                .isEqualTo(MediaType.APPLICATION_JSON);

        TransactionDto content = response.readEntity(TransactionDto.class);
        assertThat(content.getId()).isNotNull();
    }

    @Test
    public void shouldReturnListOfAllTransactions() {
        target("/transaction")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new TransactionDto(null, BigDecimal.valueOf(10),
                                UUID.fromString("cfc6ba0a-3051-4558-b110-6b8a751dfb6f"),
                                UUID.fromString("0020f419-1fac-4de7-b6e7-a47de46126b1")),
                        MediaType.APPLICATION_JSON_TYPE));
        target("/transaction")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new TransactionDto(null, BigDecimal.valueOf(10),
                                UUID.fromString("cfc6ba0a-3051-4558-b110-6b8a751dfb6f"),
                                UUID.fromString("0020f419-1fac-4de7-b6e7-a47de46126b1")),
                        MediaType.APPLICATION_JSON_TYPE));
        target("/transaction")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new TransactionDto(null, BigDecimal.valueOf(10),
                                UUID.fromString("cfc6ba0a-3051-4558-b110-6b8a751dfb6f"),
                                UUID.fromString("0020f419-1fac-4de7-b6e7-a47de46126b1")),
                        MediaType.APPLICATION_JSON_TYPE));
        Response response = target("/transaction").request().get();

        assertThat(response.getStatus())
                .as("Http Response should be 200:")
                .isEqualTo(Response.Status.OK.getStatusCode());

        assertThat(response.getHeaderString(HttpHeaders.CONTENT_TYPE))
                .as("Content type should be application/json:")
                .isEqualTo(MediaType.APPLICATION_JSON);

        List<TransactionDto> content = response.readEntity(new GenericType<List<TransactionDto>>() {
        });
        assertThat(content.size()).isGreaterThan(0);
    }

    @Test
    public void shouldFailWithNoNegativeAmountMessage() {
        Response response = target("/transaction")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new TransactionDto(null, BigDecimal.valueOf(-50),
                                UUID.fromString("cfc6ba0a-3051-4558-b110-6b8a751dfb6f"),
                                UUID.fromString("0020f419-1fac-4de7-b6e7-a47de46126b1")),
                        MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatus())
                .as("Http Response should be 400:")
                .isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        ErrorMessage errorMessage = response.readEntity(ErrorMessage.class);
        assertThat(errorMessage.getMessage())
                .as("Exception message should be:")
                .isEqualTo("negative.transactions.not.allowed");

        assertThat(errorMessage.getCode())
                .as("Exception code should be:")
                .isEqualTo(100);
    }

    @Test
    public void shouldFailWithCannotTransferToSameAccountMessage() {
        Response response = target("/transaction")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new TransactionDto(null, BigDecimal.valueOf(80),
                                UUID.fromString("0020f419-1fac-4de7-b6e7-a47de46126b1"),
                                UUID.fromString("0020f419-1fac-4de7-b6e7-a47de46126b1")),
                        MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatus())
                .as("Http Response should be 400:")
                .isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        ErrorMessage errorMessage = response.readEntity(ErrorMessage.class);
        assertThat(errorMessage.getMessage())
                .as("Exception message should be:")
                .isEqualTo("cant.transfer.to.same.account");

        assertThat(errorMessage.getCode())
                .as("Exception code should be:")
                .isEqualTo(200);
    }

    @Test
    public void shouldFailWithCannotTransferZeroAmountMessage() {
        Response response = target("/transaction")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new TransactionDto(null, BigDecimal.valueOf(0),
                                UUID.fromString("cfc6ba0a-3051-4558-b110-6b8a751dfb6f"),
                                UUID.fromString("0020f419-1fac-4de7-b6e7-a47de46126b1")),
                        MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatus())
                .as("Http Response should be 400:")
                .isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        ErrorMessage errorMessage = response.readEntity(ErrorMessage.class);
        assertThat(errorMessage.getMessage())
                .as("Exception message should be:")
                .isEqualTo("cant.transfer.zero");

        assertThat(errorMessage.getCode())
                .as("Exception code should be:")
                .isEqualTo(300);
    }

    @Test
    public void shouldFailWithNotEnoughMoneyMessage() {
        Response response = target("/transaction")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(new TransactionDto(null, BigDecimal.valueOf(100000),
                                UUID.fromString("cfc6ba0a-3051-4558-b110-6b8a751dfb6f"),
                                UUID.fromString("0020f419-1fac-4de7-b6e7-a47de46126b1")),
                        MediaType.APPLICATION_JSON_TYPE));

        assertThat(response.getStatus())
                .as("Http Response should be 400:")
                .isEqualTo(Response.Status.BAD_REQUEST.getStatusCode());

        ErrorMessage errorMessage = response.readEntity(ErrorMessage.class);
        assertThat(errorMessage.getMessage())
                .as("Exception message should be:")
                .isEqualTo("not.enough.money");

        assertThat(errorMessage.getCode())
                .as("Exception code should be:")
                .isEqualTo(AccountStateCode.NOT_ENOUGH_MONEY.getCode());
    }

    @Test
    public void shouldConcurrentlyTransferMoneyToSameAccount() throws ExecutionException, InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(100);
        Random random = new Random(System.currentTimeMillis());
        List<Future<Response>> result = random.ints(50, 0, 100)
                .boxed()
                .map(integer -> (Callable<Response>) () ->
                        target("/transaction")
                                .request(MediaType.APPLICATION_JSON_TYPE)
                                .post(Entity.entity(new TransactionDto(null, BigDecimal.valueOf(1),
                                                UUID.fromString("0020f419-1fac-4de7-b6e7-a47de46126b1"),
                                                UUID.fromString("cfc6ba0a-3051-4558-b110-6b8a751dfb6f")),
                                        MediaType.APPLICATION_JSON_TYPE)))
                .map(threadPool::submit)
                .collect(Collectors.toList());
        SoftAssertions assertions = new SoftAssertions();
        for (Future<Response> responseFuture : result) {
            Response response = responseFuture.get();
            assertions.assertThat(response.getStatus())
                    .as("Http Response should be 200:")
                    .isEqualTo(Response.Status.OK.getStatusCode());
        }
        assertions.assertAll();

        AccountDto accountTo = target("/account/cfc6ba0a-3051-4558-b110-6b8a751dfb6f").request().get()
                .readEntity(AccountDto.class);
        AccountDto accountFrom = target("/account/0020f419-1fac-4de7-b6e7-a47de46126b1").request().get()
                .readEntity(AccountDto.class);

        BigDecimal transferred = BigDecimal.valueOf(1000).subtract(accountFrom.getAmount());
        assertThat(accountTo.getAmount()).isEqualTo(BigDecimal.valueOf(500).add(transferred));
        assertThat(accountTo.getAmount()).isEqualTo(BigDecimal.valueOf(550));
        assertThat(accountFrom.getAmount()).isEqualTo(BigDecimal.valueOf(950));
    }
}
