create table transaction
(
    id            varchar(255)              not null,
    amount        decimal default 0 not null,
    fromAccountId varchar(255)              not null,
    toAccountId   varchar(255)              not null,
    primary key (id)
);
