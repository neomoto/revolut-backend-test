create table account
(
    id     varchar(255)     not null,
    amount decimal default 0 not null,
    primary key (id)
);
