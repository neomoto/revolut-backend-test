package com.revolut.transfer.controller;

import com.revolut.transfer.controller.dto.TransactionDto;
import com.revolut.transfer.controller.dto.mapper.TransactionMapper;
import com.revolut.transfer.entity.Transaction;
import com.revolut.transfer.service.TransactionService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/transaction")
public class TransactionController {
    private TransactionMapper transactionMapper = new TransactionMapper();
    private TransactionService transactionService = new TransactionService();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createTransaction(TransactionDto transactionDto) {
        Transaction transaction = transactionService.createTransaction(transactionMapper.fromDto(transactionDto));
        return Response.ok(transactionMapper.toDto(transaction)).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        List<Transaction> transaction = transactionService.findAll();
        return Response.ok(transactionMapper.toDtoList(transaction)).build();
    }
}
