package com.revolut.transfer.controller;

import com.revolut.transfer.controller.dto.mapper.AccountMapper;
import com.revolut.transfer.entity.Account;
import com.revolut.transfer.service.AccountService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

@Path("/account")
public class AccountController {
    private AccountService accountService = new AccountService();
    private AccountMapper accountMapper = new AccountMapper();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccounts() {
        List<Account> accounts = accountService.getAllAccounts();
        return Response.ok(accountMapper.toDtoList(accounts)).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccount(@PathParam("id") UUID id) {
        Account account = accountService.getAccount(id);
        return Response.ok(accountMapper.toDto(account)).build();
    }

}
