package com.revolut.transfer.controller.exception.mapper;

import com.revolut.transfer.controller.dto.ErrorMessage;
import com.revolut.transfer.service.exception.ServiceException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ServiceExceptionMapper implements ExceptionMapper<ServiceException> {
    @Override
    public Response toResponse(ServiceException e) {
        return Response.status(Response.Status.BAD_REQUEST)
            .entity(new ErrorMessage(e.getMessage(), e.getCode()))
            .build();
    }
}
