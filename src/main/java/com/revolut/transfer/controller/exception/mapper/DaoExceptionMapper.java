package com.revolut.transfer.controller.exception.mapper;

import com.revolut.transfer.controller.dto.ErrorMessage;
import com.revolut.transfer.dao.exception.DaoException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DaoExceptionMapper implements ExceptionMapper<DaoException> {
    @Override
    public Response toResponse(DaoException e) {
        return Response.status(Response.Status.BAD_REQUEST)
            .entity(new ErrorMessage(e.getMessage(), e.getCode()))
            .build();
    }
}
