package com.revolut.transfer.controller.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

public interface FromDtoMapper<T, E> {
    E fromDto(T dto);
}
