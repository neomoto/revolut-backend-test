package com.revolut.transfer.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TransactionDto {
    private UUID id;
    private BigDecimal amount;
    private UUID fromAccountId;
    private UUID toAccountId;
}
