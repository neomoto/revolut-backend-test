package com.revolut.transfer.controller.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

public interface ToDtoMapper<T, E> {
    T toDto(E entity);

    default List<T> toDtoList(List<E> elementList) {
        return elementList.stream().map(this::toDto).collect(Collectors.toList());
    }
}
