package com.revolut.transfer.controller.dto.mapper;

public interface TwoWayDtoMapper<T, E> extends FromDtoMapper<T, E>, ToDtoMapper<T, E> {

}
