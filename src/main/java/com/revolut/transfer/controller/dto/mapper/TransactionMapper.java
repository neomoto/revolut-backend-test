package com.revolut.transfer.controller.dto.mapper;

import com.revolut.transfer.controller.dto.TransactionDto;
import com.revolut.transfer.entity.Transaction;

public class TransactionMapper implements TwoWayDtoMapper<TransactionDto, Transaction> {
    @Override
    public TransactionDto toDto(Transaction entity) {
        return new TransactionDto(entity.getId(), entity.getAmount(),
                                  entity.getFromAccountId(), entity.getToAccountId());
    }

    @Override
    public Transaction fromDto(TransactionDto dto) {
        return new Transaction(dto.getId(), dto.getAmount(),
                               dto.getFromAccountId(), dto.getToAccountId());
    }
}
