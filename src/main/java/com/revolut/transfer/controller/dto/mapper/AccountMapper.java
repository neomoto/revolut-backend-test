package com.revolut.transfer.controller.dto.mapper;

import com.revolut.transfer.controller.dto.AccountDto;
import com.revolut.transfer.entity.Account;

public class AccountMapper implements TwoWayDtoMapper<AccountDto, Account> {
    @Override
    public AccountDto toDto(Account entity) {
        return new AccountDto(entity.getId(), entity.getAmount());
    }

    @Override
    public Account fromDto(AccountDto dto) {
        return new Account(dto.getId(), dto.getAmount());
    }
}
