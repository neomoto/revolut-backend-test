package com.revolut.transfer.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@AllArgsConstructor
public class Transaction {
    private UUID id;
    private BigDecimal amount;
    private UUID fromAccountId;
    private UUID toAccountId;
}
