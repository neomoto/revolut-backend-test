package com.revolut.transfer.service;

import com.revolut.transfer.dao.AccountDao;
import com.revolut.transfer.entity.Account;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.UUID;

@Slf4j
public class AccountService {

    private AccountDao accountDao = new AccountDao();

    /**
     * Get account by id.
     *
     * @param id {@code UUID} id of account
     * @return {@code Account} account
     */
    public Account getAccount(UUID id) {
        return accountDao.getAccount(id);
    }

    /**
     * Get all existed accounts.
     *
     * @return {@code List<Account>} account list
     */
    public List<Account> getAllAccounts() {
        return accountDao.getAll();
    }
}
