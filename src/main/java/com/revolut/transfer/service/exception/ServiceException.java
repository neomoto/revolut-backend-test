package com.revolut.transfer.service.exception;

import lombok.Getter;

/**
 * Service layer generic exception.
 */
public class ServiceException extends RuntimeException {
    @Getter
    private int code;

    public ServiceException(String message, int code) {
        super(message);
        this.code = code;
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
