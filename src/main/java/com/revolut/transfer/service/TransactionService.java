package com.revolut.transfer.service;

import com.revolut.transfer.dao.TransactionDao;
import com.revolut.transfer.dao.exception.DaoException;
import com.revolut.transfer.entity.Transaction;
import com.revolut.transfer.service.exception.ServiceException;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class TransactionService {
    private final int RETRY_COUNT = 3;
    private TransactionDao transactionDao = new TransactionDao();

    /**
     * Saves transaction with predefined amount of attempts.
     * Due to locks in database during multithreaded requests application should be
     * prepared to database exceptions regarding locks.
     *
     * @param transactionRequest {@code Transaction} transaction
     * @param attempt            Attempt
     * @return saved transaction
     */
    private Transaction createTransaction(Transaction transactionRequest, int attempt) {
        if (transactionRequest.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new ServiceException("negative.transactions.not.allowed", 100);
        }
        if (transactionRequest.getFromAccountId().equals(transactionRequest.getToAccountId())) {
            throw new ServiceException("cant.transfer.to.same.account", 200);
        }
        if (transactionRequest.getAmount().equals(BigDecimal.ZERO)) {
            throw new ServiceException("cant.transfer.zero", 300);
        }
        Transaction transaction = new Transaction(UUID.randomUUID(),
                                                  transactionRequest.getAmount(),
                                                  transactionRequest.getFromAccountId(),
                                                  transactionRequest.getToAccountId());
        try {
            transactionDao.makeTransfer(transaction);
        }
        catch (DaoException e) {
            if (attempt < RETRY_COUNT) {
                createTransaction(transactionRequest, attempt + 1);
            }
            else {
                throw new ServiceException(e.getMessage(), e.getCode());
            }
        }

        return transaction;
    }

    /**
     * Create transaction.
     *
     * @param transactionRequest {@code Transaction} transaction
     * @return saved transaction
     */
    public Transaction createTransaction(Transaction transactionRequest) {
        return createTransaction(transactionRequest, 0);
    }

    /**
     * Returns list of all transactions
     *
     * @return {@code List<Transaction>} transactions
     */
    public List<Transaction> findAll() {
        return transactionDao.findAll();
    }
}
