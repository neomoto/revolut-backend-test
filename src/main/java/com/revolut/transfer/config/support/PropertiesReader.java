package com.revolut.transfer.config.support;

import com.revolut.transfer.config.ApplicationConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;

public class PropertiesReader {
    private final String propertiesPath;

    public PropertiesReader(String propertiesPath) {
        this.propertiesPath = propertiesPath;
    }

    public ApplicationConfiguration getApplicationConfiguration() throws ConfigurationException {
        Configuration configuration = readApplicationConfiguration();
        return new ApplicationConfiguration(
                configuration.getInt(ApplicationProperties.APPLICATION_SERVER_PORT),
                configuration.getString(ApplicationProperties.BASE_CONTEXT_PATH)
        );
    }

    private Configuration readApplicationConfiguration() throws ConfigurationException {
        Configurations configs = new Configurations();
        return configs.properties(new File(this.propertiesPath));
    }
}
