package com.revolut.transfer.config.support;

public final class ApplicationProperties {
    public static final String APPLICATION_SERVER_PORT = "server.port";
    public static final String BASE_CONTEXT_PATH = "server.context";

    private ApplicationProperties() {
    }
}
