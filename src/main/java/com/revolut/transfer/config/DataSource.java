package com.revolut.transfer.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.Flyway;

import java.sql.Connection;
import java.sql.SQLException;

@Slf4j
public class DataSource {
    private static HikariConfig config = new HikariConfig("/datasource.properties");
    private static HikariDataSource dataSource;

    static {
        dataSource = new HikariDataSource(config);
        Flyway flyway = Flyway.configure()
            .dataSource(config.getJdbcUrl(), config.getUsername(), config.getPassword())
            .load();
        flyway.migrate();
    }

    public static Connection getConnection() {
        try {
            return dataSource.getConnection();
        }
        catch (SQLException e) {
            log.error("Couldn't get database connection", e);
            throw new RuntimeException("Couldn't get database connection", e);
        }
    }

    public static void checkConnAlive() {
        log.debug("Checking database availability");
        try (Connection connection = getConnection()) {
            connection.createStatement().execute("select 1");
            log.debug("Database available");
        }
        catch (SQLException | RuntimeException e) {
            log.debug("Database is not available!");
            throw new RuntimeException("Database is not available!");
        }
    }
}
