package com.revolut.transfer.config;

public class ApplicationConfiguration {
    private final int port;
    private final String context;

    public ApplicationConfiguration(int port, String context) {
        this.port = port;
        this.context = context;
    }

    public int getPort() {
        return port;
    }

    public String getContext() {
        return context;
    }
}
