package com.revolut.transfer.dao.exception;

public class IncorrectAccountStateException extends DaoException {
    public IncorrectAccountStateException(String message, int code) {
        super(message, code);
    }
}
