package com.revolut.transfer.dao.exception;

import lombok.Getter;

/**
 * DAO layer generic exception.
 */
public class DaoException extends RuntimeException {
    @Getter
    private int code;

    public DaoException(String message, int code) {
        super(message);
        this.code = code;
    }

    public DaoException(String message, Throwable cause, int code) {
        super(message, cause);
        this.code = code;
    }
}
