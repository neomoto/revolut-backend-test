package com.revolut.transfer.dao.exception;

public class EntityNotFoundException extends DaoException {
    public EntityNotFoundException(String message, int code) {
        super(message, code);
    }

    public EntityNotFoundException(String message, Throwable cause, int code) {
        super(message, cause, code);
    }
}
