package com.revolut.transfer.dao;

import com.revolut.transfer.config.DataSource;
import com.revolut.transfer.dao.exception.EntityNotFoundException;
import com.revolut.transfer.entity.Account;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
public class AccountDao {
    public Account getAccount(UUID id) {
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select * from account where id = ?");
            statement.setString(1, id.toString());
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return new Account(UUID.fromString(rs.getString("id")),
                        rs.getBigDecimal("amount"));
            }
            else {
                log.error("Account {} not found", id);
                throw new EntityNotFoundException("account.not.found", 404);
            }
        } catch (SQLException e) {
            log.error("sql exception while fetching account", e);
            throw new EntityNotFoundException("account.not.found", e, 404);
        }
    }

    public List<Account> getAll() {
        List<Account> accounts = new ArrayList<>();
        try (Connection connection = DataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select * from account");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                accounts.add(new Account(UUID.fromString(rs.getString("id")),
                        rs.getBigDecimal("amount")));
            }
        } catch (SQLException e) {
            log.error("sql exception while fetching accounts", e);
            throw new EntityNotFoundException("account.list.not.found", e, 404);
        }
        return accounts;
    }
}
