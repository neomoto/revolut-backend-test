package com.revolut.transfer.dao;

import com.revolut.transfer.config.DataSource;
import com.revolut.transfer.dao.enums.AccountStateCode;
import com.revolut.transfer.dao.exception.DaoException;
import com.revolut.transfer.dao.exception.EntityNotFoundException;
import com.revolut.transfer.dao.exception.IncorrectAccountStateException;
import com.revolut.transfer.entity.Account;
import com.revolut.transfer.entity.Transaction;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
public class TransactionDao {
    private List<UUID> transactions = new ArrayList<>();

    public void makeTransfer(Transaction transaction) {
        try (Connection connection = DataSource.getConnection()) {
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);

            Account toAccount = findAccount(connection, transaction.getToAccountId());
            Account fromAccount = findAccount(connection, transaction.getFromAccountId());

            if (fromAccount.getAmount().compareTo(transaction.getAmount()) < 0) {
                log.error("Attempt to transfer amount larger then available");
                throw new IncorrectAccountStateException("not.enough.money",
                        AccountStateCode.NOT_ENOUGH_MONEY.getCode());
            }

            updateAmount(connection, transaction.getFromAccountId(),
                    fromAccount.getAmount().subtract(transaction.getAmount()));

            updateAmount(connection, transaction.getToAccountId(),
                    toAccount.getAmount().add(transaction.getAmount()));

            saveTransaction(connection, transaction);
            connection.commit();
        } catch (SQLException e) {
            log.error("sql exception while fetching account", e);
            throw new DaoException(e.getMessage(), e.getErrorCode());
        }
    }

    private Account findAccount(Connection connection, UUID accountId) throws SQLException {
        String findAccountSql = "select * from account where id = ? for update";
        PreparedStatement checkAccountsStatement = connection.prepareStatement(findAccountSql);

        checkAccountsStatement.setString(1, accountId.toString());
        ResultSet rs = checkAccountsStatement.executeQuery();
        if (!rs.next()) {
            checkAccountsStatement.close();
            connection.rollback();
            log.error("Account from {} not found", accountId);
            throw new EntityNotFoundException("account.from.not.found", 404);
        }

        return new Account(UUID.fromString(rs.getString("id")),
                rs.getBigDecimal("amount"));
    }

    private void saveTransaction(Connection connection, Transaction transaction) throws SQLException {
        UUID fromAccountId = transaction.getFromAccountId();
        UUID toAccountId = transaction.getToAccountId();
        if (transactions.contains(transaction.getId())) {
            System.out.println("FOUND BITCH!");
        }
        transactions.add(transaction.getId());
        //language=SQL
        String saveTransactionSql = "insert into transaction (id, amount, fromAccountId, toAccountId) " +
                "values (?, ?, ?, ?)";

        PreparedStatement removeStatement = connection.prepareStatement(saveTransactionSql);
        removeStatement.setString(1, transaction.getId().toString());
        removeStatement.setBigDecimal(2, transaction.getAmount());
        removeStatement.setString(3, fromAccountId.toString());
        removeStatement.setString(4, toAccountId.toString());
        removeStatement.executeUpdate();
    }

    private void updateAmount(Connection connection, UUID accountId, BigDecimal amount) throws SQLException {
        //language=SQL
        String updateMoney = "update account " +
                "set amount = ? " +
                "where id = ? ";
        PreparedStatement statement = connection.prepareStatement(updateMoney);
        statement.setBigDecimal(1, amount);
        statement.setString(2, accountId.toString());
        statement.executeUpdate();
        statement.close();
    }

    public List<Transaction> findAll() {
        List<Transaction> result = new ArrayList<>();
        try (Connection connection = DataSource.getConnection()) {
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement("select * from transaction");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                result.add(new Transaction(
                        UUID.fromString(resultSet.getString("id")),
                        resultSet.getBigDecimal("amount"),
                        UUID.fromString(resultSet.getString("fromAccountId")),
                        UUID.fromString(resultSet.getString("toAccountId"))
                ));
            }
            connection.commit();
        } catch (SQLException e) {
            log.error("sql exception while fetching account", e);
            throw new EntityNotFoundException("transaction.list.not.found", e, 404);
        }
        return result;
    }
}
