package com.revolut.transfer.dao.enums;

import lombok.Getter;

public enum AccountStateCode {
    NOT_ENOUGH_MONEY(5002),
    ;

    @Getter
    private int code;

    AccountStateCode(int code) {
        this.code = code;
    }
}
