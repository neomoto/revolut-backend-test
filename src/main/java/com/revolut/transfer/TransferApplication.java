package com.revolut.transfer;

import com.revolut.transfer.config.ApplicationConfiguration;
import com.revolut.transfer.config.DataSource;
import com.revolut.transfer.config.support.PropertiesReader;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

public class TransferApplication {
    public static void main(String[] args) throws Exception {
        TransferApplication application = new TransferApplication();
        application.start(args);
    }

    public void start(String[] args) throws Exception {
        String propertiesFilePath = "application.properties";
        if (args.length > 0) {
            String[] propertiesPath = args[0].split("=");
            if ("config.location".equals(propertiesPath[0])) {
                propertiesFilePath = propertiesPath[1];
            }
        }

        Server server = configureServer(propertiesFilePath);

        DataSource.checkConnAlive();
        try {
            server.start();
            server.join();
        } finally {
            server.destroy();
        }
    }

    private ApplicationConfiguration getProperties(String propertiesFilePath) throws ConfigurationException {
        PropertiesReader propertiesReader = new PropertiesReader(propertiesFilePath);
        return propertiesReader.getApplicationConfiguration();
    }

    private Server configureServer(String propertiesFilePath) throws ConfigurationException {
        ApplicationConfiguration appConfig = getProperties(propertiesFilePath);
        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.packages("com.revolut.transfer.controller");
        ServletHolder jerseyServlet = new ServletHolder(new ServletContainer(resourceConfig));
        Server jettyServer = new Server(appConfig.getPort());
        ServletContextHandler context = new ServletContextHandler(jettyServer, appConfig.getContext());
        context.addServlet(jerseyServlet, "/*");
        jerseyServlet.setInitOrder(0);

        return jettyServer;
    }
}
